# scheme-management-ui

The Scheme Management UI to maintain the scheme.

### Launch Screen generation

To generate the Launch Screens, change settings in pubspec.yaml and run

```sh
dart run flutter_native_splash:create
```