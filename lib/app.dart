// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:scheme_management_ui/screens/main/main_screen.dart';
// import 'package:scheme_management_ui/theme.dart';
import 'package:flutter/material.dart';

class SchemeManagerApp extends StatelessWidget {
  const SchemeManagerApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: 'Blauwe Knop Stelselbeheer',
      debugShowCheckedModeBanner: false,
      // theme: UserTheme.blauweknop(),
      home: MainScreen(),
    );
  }
}
