// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:scheme_management_ui/models/organization.dart';

abstract class SchemeClient {
  Stream<List<Organization>> listOrganizations();
}
