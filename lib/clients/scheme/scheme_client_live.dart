// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:scheme_management_ui/models/logging/device_event_class_id.dart';
import 'package:scheme_management_ui/models/organization.dart';
import 'package:http/http.dart' as http;
import 'package:scheme_management_ui/providers/env_provider.dart';
import 'package:scheme_management_ui/helpers/logger.dart';
import 'dart:async';
import 'dart:convert';

import 'scheme_client.dart';

class SchemeClientLive implements SchemeClient {
  final EnvProvider envProvider;

  SchemeClientLive(this.envProvider);

  @override
  Stream<List<Organization>> listOrganizations() async* {
    var env = await envProvider.getEnv();
    final response = await http.get(
      Uri.parse('${env.schemeBaseUrl}/fetch_organizations'),
    );

    if (response.statusCode == 200) {
      var responseJson = json.decode(response.body) as List;

      var result = responseJson.map((i) => Organization.fromJson(i)).toList();

      yield result;
    } else {
      Logger.addApiLog(
        type: DeviceEventClassId.smu_1,
        message: 'Failed to get organizations',
        action: 'Fetching Organizations',
        protocol: 'SchemeClientLive',
        requestUri: '${env.schemeBaseUrl}/fetch_organizations',
        requestMethod: 'GET',
      );

      throw Exception('Failed to get organizations');
    }
  }
}
