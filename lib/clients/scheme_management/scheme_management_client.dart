// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

abstract class SchemeManagementClient {
  Future<void> approveOrganization(String oin);
  Future<void> deleteOrganization(String oin);
}
