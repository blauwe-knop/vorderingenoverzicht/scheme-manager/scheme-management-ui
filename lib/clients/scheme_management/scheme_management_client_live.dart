// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:http/http.dart' as http;
import 'package:scheme_management_ui/helpers/logger.dart';
import 'package:scheme_management_ui/models/logging/device_event_class_id.dart';
import 'package:scheme_management_ui/providers/env_provider.dart';
import 'dart:async';

import 'scheme_management_client.dart';

class SchemeManagementClientLive implements SchemeManagementClient {
  final EnvProvider envProvider;

  SchemeManagementClientLive(this.envProvider);

  @override
  Future<void> approveOrganization(String oin) async {
    var env = await envProvider.getEnv();
    final response = await http.post(
      Uri.parse('${env.schemeManagementBaseUrl}/approve_organization/$oin'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
    );

    if (response.statusCode != 200) {
      Logger.addApiLog(
        type: DeviceEventClassId.smu_2,
        message: 'Failed to approve organization: $oin',
        action: 'Approve Organizations',
        protocol: 'SchemeManagementClientLive',
        requestUri: '${env.schemeManagementBaseUrl}/approve_organization/$oin',
        requestMethod: 'POST',
      );

      throw Exception('Failed to register scheme');
    }
  }

  @override
  Future<void> deleteOrganization(String oin) async {
    var env = await envProvider.getEnv();
    final response = await http.delete(
      Uri.parse('${env.schemeManagementBaseUrl}/delete_organization/$oin'),
    );

    if (response.statusCode != 200) {
      Logger.addApiLog(
        type: DeviceEventClassId.smu_3,
        message: 'Failed to delete organization: $oin',
        action: 'Delete Organizations',
        protocol: 'SchemeManagementClientLive',
        requestUri: '${env.schemeManagementBaseUrl}/delete_organization/$oin',
        requestMethod: 'DELETE',
      );

      throw Exception('Failed to register scheme');
    }
  }
}
