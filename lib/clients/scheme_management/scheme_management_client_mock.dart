// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:scheme_management_ui/clients/scheme/scheme_client_mock.dart';

import 'scheme_management_client.dart';

class SchemeManagementClientMock implements SchemeManagementClient {
  @override
  Future<void> approveOrganization(String oin) {
    organizationList[0].approved = !organizationList[0].approved;
    return Future.value(null);
  }

  @override
  Future<void> deleteOrganization(String oin) {
    organizationList.removeWhere((element) => element.oin == oin);
    return Future.value(null);
  }
}
