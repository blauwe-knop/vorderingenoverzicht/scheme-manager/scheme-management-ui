import 'dart:developer';

import 'package:scheme_management_ui/models/logging/device_event_class_id.dart';

class Logger {
  static void addApiLog({
    required DeviceEventClassId type,
    required String message,
    required String action,
    required String protocol,
    required String requestUri,
    required String requestMethod,
  }) {
    log(_createCEFString(type.name, type.eventName, type.severityLevel.value,
        message, action, protocol, requestUri, requestMethod));
  }

  static String _createCEFString(
      String eventClassId,
      String eventName,
      int severityLevel,
      String errorMessage,
      String action,
      String applicationProtocol,
      String requestUri,
      String method) {
    var timestamp = DateTime.now().toIso8601String();
    var host = "TODO"; //TODO: Need to add host
    var cef = "CEF:1";
    var vendor = "VORIJK";
    var product = "scheme_management_ui";
    var version = "TODO"; //TODO: Need to add version number
    var deviceEventClassId = eventClassId;
    var name = eventName;
    var severity = severityLevel;
    var flexString1Label = "error message";
    var flexString1 = errorMessage;
    var flexString2Label = "device";
    var flexString2 = "Web";
    var act = action;
    var app = applicationProtocol;
    var request = requestUri;
    var requestMethod = method;
    return "$timestamp $host $cef|$vendor|$product|$version|$deviceEventClassId|$name|"
        "$severity|$flexString1Label|$flexString1|$flexString2Label|$flexString2|"
        "$act|$app|$request|$requestMethod";
  }
}
