// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:scheme_management_ui/providers/env_provider.dart';
import 'package:scheme_management_ui/providers/menu_provider.dart';
import 'package:scheme_management_ui/providers/organization_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'app.dart';
import 'clients/scheme/scheme_client_live.dart';
import 'clients/scheme_management/scheme_management_client_live.dart';

void main() {
  runApp(
    MultiProvider(
      providers: [
        Provider<EnvProvider>(create: (_) => EnvProvider()),
        ChangeNotifierProvider<MenuProvider>(
          create: (_) => MenuProvider(),
        ),
        ChangeNotifierProvider<OrganizationsProvider>(
          create: (context) {
            var organizationsProvider = OrganizationsProvider(
                SchemeClientLive(context.read<EnvProvider>()),
                SchemeManagementClientLive(context.read<EnvProvider>()));
            organizationsProvider.get();
            return organizationsProvider;
          },
        ),
      ],
      child: const SchemeManagerApp(),
    ),
  );
}
