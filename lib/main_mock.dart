// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:scheme_management_ui/providers/menu_provider.dart';
import 'package:scheme_management_ui/providers/organization_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'app.dart';
import 'clients/scheme/scheme_client_mock.dart';
import 'clients/scheme_management/scheme_management_client_mock.dart';

void main() {
  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider<MenuProvider>(
          create: (_) => MenuProvider(),
        ),
        ChangeNotifierProvider<OrganizationsProvider>(
          create: (_) => OrganizationsProvider(
            SchemeClientMock(),
            SchemeManagementClientMock(),
          ),
        ),
      ],
      child: const SchemeManagerApp(),
    ),
  );
}
