import 'package:scheme_management_ui/models/logging/event_severity_level.dart';

enum DeviceEventClassId {
  smu_1('FailedToGetOrganizations', EventSeverityLevel.medium),
  smu_2('FailedToApproveOrganization', EventSeverityLevel.high),
  smu_3('FailedToDeleteOrganization', EventSeverityLevel.high);

  const DeviceEventClassId(this.eventName, this.severityLevel);

  final String eventName;
  final EventSeverityLevel severityLevel;
}
