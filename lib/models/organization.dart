// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

class Organization {
  String oin;
  String name;
  Uri discoveryUrl;
  String publicKey;
  bool approved;
  String? logoUrl;

  Organization({
    required this.oin,
    required this.name,
    required this.discoveryUrl,
    required this.publicKey,
    required this.approved,
    this.logoUrl,
  });

  factory Organization.fromJson(Map<String, dynamic> jsonObject) {
    return Organization(
        oin: jsonObject["oin"],
        name: jsonObject["name"],
        discoveryUrl: Uri.parse(jsonObject["discoveryUrl"]),
        publicKey: jsonObject["publicKey"],
        approved: jsonObject["approved"],
        logoUrl: jsonObject["logoUrl"]);
  }

  Map<String, dynamic> toJson() => {
        'oin': oin,
        'name': name,
        'discoveryUrl': discoveryUrl.toString(),
        'publicKey': publicKey,
        'approved': approved,
        'logoUrl': logoUrl,
      };
}
