// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'dart:convert';
import 'package:flutter/services.dart' show rootBundle;

class Env {
  String? schemeBaseUrl;
  String? schemeManagementBaseUrl;
}

class EnvProvider {
  bool _isLoaded = false;
  final Env _env = Env();

  Future<Env> getEnv() async {
    await loadEnvJson();
    return _env;
  }

  Future<void> loadEnvJson() async {
    if (!_isLoaded) {
      var response = await rootBundle.loadString('assets/env.json');
      var responseJson = json.decode(response);

      _env.schemeBaseUrl = responseJson["schemeBaseUrl"];
      _env.schemeManagementBaseUrl = responseJson["schemeManagementBaseUrl"];

      _isLoaded = true;
    }
  }
}
