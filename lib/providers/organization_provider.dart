// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'dart:async';

import 'package:scheme_management_ui/clients/scheme/scheme_client.dart';
import 'package:scheme_management_ui/clients/scheme_management/scheme_management_client.dart';
import 'package:scheme_management_ui/models/organization.dart';
import 'package:flutter/material.dart';

class OrganizationsProvider extends ChangeNotifier {
  OrganizationsProvider(this._schemeClient, this._schemeManagementClient);

  final SchemeClient _schemeClient;
  final SchemeManagementClient _schemeManagementClient;
  List<Organization> organizations = [];

  void get() {
    var data = _schemeClient.listOrganizations();

    data.listen((event) {
      organizations = event;

      notifyListeners();
    });
  }

  Future<void> fetchGetAll() async {
    get();
  }

  Future<void> approveOrganization(String oin) async {
    await _schemeManagementClient.approveOrganization(oin);
    get();
  }

  Future<void> deleteOrganization(String oin) async {
    await _schemeManagementClient.deleteOrganization(oin);
    get();
  }
}
