// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';
import 'package:scheme_management_ui/constants.dart';
import 'package:scheme_management_ui/models/organization.dart';
import 'package:scheme_management_ui/providers/organization_provider.dart';
import 'package:flutter/material.dart';

class OrganizationListItem extends StatelessWidget {
  final Organization organization;

  const OrganizationListItem({
    required this.organization,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(defaultPadding),
      decoration: const BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(10)),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              SvgPicture.network(
                  width: 48,
                  height: 48,
                  organization.logoUrl ??
                      "https://blauweknop.app/images/bk_logo.svg",
                  semanticsLabel: 'Blauwe Knop Logo'),
              const Spacer(),
              Flexible(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      organization.name,
                      style: Theme.of(context).textTheme.titleLarge,
                    ),
                    Text(
                      "OIN: ${organization.oin}",
                      style: Theme.of(context).textTheme.bodyLarge,
                    ),
                  ],
                ),
              ),
            ],
          ),
          const SizedBox(height: 10),
          const Divider(),
          const SizedBox(height: 10),
          Row(
            children: [
              // Approval
              Expanded(
                child: Column(
                  children: [
                    if (!organization.approved)
                      OutlinedButton(
                        style: OutlinedButton.styleFrom(
                            backgroundColor: Colors.green[600],
                            foregroundColor: Colors.white,
                            textStyle: const TextStyle(color: Colors.white)),
                        onPressed: () {
                          Provider.of<OrganizationsProvider>(context,
                                  listen: false)
                              .approveOrganization(organization.oin);

                          const snackBar = SnackBar(
                            content: Text('Organisatie goedgekeurd.'),
                          );
                          ScaffoldMessenger.of(context).showSnackBar(snackBar);
                        },
                        child: const Text("Goedkeuren"),
                      )
                    else
                      const Text("Goedgekeurd."),
                  ],
                ),
              ),
              // Removal
              Expanded(
                child: Column(
                  children: [
                    OutlinedButton(
                      style: OutlinedButton.styleFrom(
                          backgroundColor: Colors.red[300],
                          foregroundColor: Colors.white,
                          textStyle: const TextStyle(color: Colors.white)),
                      onPressed: () {
                        Provider.of<OrganizationsProvider>(context,
                                listen: false)
                            .deleteOrganization(organization.oin)
                            .whenComplete(() {
                          const snackBar = SnackBar(
                            content: Text('Organisatie verwijderd.'),
                          );
                          ScaffoldMessenger.of(context).showSnackBar(snackBar);
                        });
                      },
                      child: const Text("Verwijderen"),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
