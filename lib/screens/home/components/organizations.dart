// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:scheme_management_ui/providers/organization_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:scheme_management_ui/screens/home/components/organization_list_item.dart';

class Organizations extends StatelessWidget {
  const Organizations({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final organizations =
        Provider.of<OrganizationsProvider>(context).organizations;

    if (organizations.isNotEmpty) {
      return ListView.separated(
        padding: const EdgeInsets.all(8),
        itemCount: organizations.length,
        itemBuilder: (BuildContext context, int index) {
          return OrganizationListItem(
            organization: organizations[index],
          );
        },
        separatorBuilder: (BuildContext context, int index) => const SizedBox(
          height: 10,
        ),
      );
    } else {
      return Text(
        "Geen organisaties gevonden.",
        style: Theme.of(context).textTheme.titleLarge,
      );
    }
  }
}
