// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:scheme_management_ui/constants.dart';
import 'package:scheme_management_ui/screens/home/components/header.dart';
import 'package:scheme_management_ui/screens/home/components/organizations.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);
  static const routePath = '/';

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        color: const Color.fromARGB(255, 250, 222, 222),
        padding: const EdgeInsets.all(defaultPadding),
        child: const Column(
          children: [
            Header(),
            SizedBox(height: defaultPadding),
            Expanded(
              child: Organizations(),
            )
          ],
        ),
      ),
    );
  }
}
