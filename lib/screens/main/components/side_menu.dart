// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:scheme_management_ui/screens/home/home_screen.dart';

class SideMenu extends StatelessWidget {
  const SideMenu({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: [
          DrawerHeader(
            child: Column(
              children: [
                SvgPicture.asset(
                    width: 100,
                    height: 100,
                    "assets/images/bk_logo.svg",
                    semanticsLabel: 'Blauwe Knop Logo'),
                const Text(
                  "Blauwe Knop Stelselbeheer",
                  textAlign: TextAlign.center,
                ),
              ],
            ),
          ),
          DrawerListTile(
            title: "Stelselbeheer",
            press: () => Navigator.of(context)
                .pushReplacementNamed(HomeScreen.routePath),
          ),
        ],
      ),
    );
  }
}

class DrawerListTile extends StatelessWidget {
  const DrawerListTile({
    Key? key,
    required this.title,
    required this.press,
  }) : super(key: key);

  final String title;
  final VoidCallback press;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: press,
      horizontalTitleGap: 0.0,
      title: Text(
        title,
      ),
    );
  }
}
