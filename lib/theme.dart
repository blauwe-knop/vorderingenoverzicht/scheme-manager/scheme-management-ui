// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:flutter/material.dart';

class UserTheme {
  static ThemeData? blauweknop() {
    return ThemeData(
        fontFamily: 'OpenSans',
        primaryColor: const Color(0xff0b71a1),
        appBarTheme: const AppBarTheme(
          backgroundColor: Color(0xff0b71a1),
        ),
        textTheme: const TextTheme(
          bodyLarge: TextStyle(
            color: Color(0xff696969),
            fontSize: 14,
            fontWeight: FontWeight.normal,
          ),
          bodyMedium: TextStyle(
            color: Color(0xff212121),
            fontSize: 16,
            fontWeight: FontWeight.normal,
          ),
          displayLarge: TextStyle(
            color: Color(0xff212121),
            fontSize: 20,
            fontWeight: FontWeight.w600,
            height: 1.3,
          ),
          labelLarge: TextStyle(
            fontFamily: 'OpenSans',
            color: Colors.white,
            fontWeight: FontWeight.w400,
            fontSize: 20,
          ),
        ),
        buttonTheme: const ButtonThemeData(
          buttonColor: Color(0xff0b71a1),
          textTheme: ButtonTextTheme.primary,
        ),
        iconTheme: const IconThemeData(color: Color(0xff757575), size: 14),
        colorScheme: ColorScheme.fromSwatch()
            .copyWith(surface: const Color(0xffeeeeee))
            .copyWith(error: const Color(0xfff02b41)));
  }
}
