import 'package:flutter_test/flutter_test.dart';
import 'package:scheme_management_ui/models/logging/device_event_class_id.dart';
import 'package:scheme_management_ui/models/logging/event_severity_level.dart';

void main() {
  test('EventType test', () async {
    // Arrange
    const eventType = DeviceEventClassId.smu_1;
    // Assert
    expect(eventType.name, "smu_1");
    expect(eventType.eventName, "FailedToGetOrganizations");
    expect(eventType.severityLevel, EventSeverityLevel.medium);
  });
}
