// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:scheme_management_ui/app.dart';
import 'package:scheme_management_ui/clients/scheme/scheme_client_mock.dart';
import 'package:scheme_management_ui/clients/scheme_management/scheme_management_client_mock.dart';
import 'package:scheme_management_ui/providers/menu_provider.dart';
import 'package:scheme_management_ui/providers/organization_provider.dart';
import 'package:scheme_management_ui/screens/main/main_screen.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:provider/provider.dart';

void main() {
  testWidgets('Mainscreen starts on startup', (WidgetTester tester) async {
    await tester.pumpWidget(
      MultiProvider(
        providers: [
          ChangeNotifierProvider<MenuProvider>(
            create: (_) => MenuProvider(),
          ),
          ChangeNotifierProvider<OrganizationsProvider>(
            create: (_) => OrganizationsProvider(
              SchemeClientMock(),
              SchemeManagementClientMock(),
            ),
          ),
        ],
        child: const SchemeManagerApp(),
      ),
    );

    final mainScreenFinder = find.byType(MainScreen);

    expect(mainScreenFinder, findsOneWidget);
  });
}
